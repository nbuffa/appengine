FROM golang:1.13-alpine

COPY sa.json .

ENV GOOGLE_APPLICATION_CREDENTIALS=sa.json
ENV MINIO_ACCESS_KEY=op-rate
ENV MINIO_SECRET_KEY=op-rate
ENV MINIO_PORT: 8080

ENTRYPOINT ["minio", "gateway", "gcs", "op-rate-labs"]